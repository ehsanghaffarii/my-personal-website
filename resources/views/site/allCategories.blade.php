@extends('site.layouts.app')
@section('title', 'دسته‌بندی مقالات برنامه‌نویسی وب | وبسایت احسان غفارلنگرودی')
@section('meta_description', 'در این بخش مقاله‌های سایت را در دسته‌بندی‌های مختلف قرار داده‌ام، مقاله‌های آموزش برنامه‌نویسی و طراحی وبسایت')

@section('content')
<main>
<div class="container medium">
    <section>
        <h2> دسته بندی‌ها </h2>
        <div class="grid guide">
            @foreach ($categories as $category)
            <a class="btn btn-primary" href="{{ $category->getLink() }}">{{ $category->name}}</a>
            @endforeach
        </div>
    </section>
</div>
</main>
@stop
