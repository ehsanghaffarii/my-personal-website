@extends('site.layouts.app')
@section('title', 'دسته‌بندی مقالات توسعه‌ی وب')
@section('meta_description', 'مقالات مربوط به طراحی، راه‌اندازی و توسعه‌ی وبسایت')

@section('content')
<main>
    <div class="container medium">
        <section>
            @include('site.layouts.header-section')
        </section>
        <section>
            <h2> همه‌ی مقالات {{ $category->name }}</h2>
            <div class="grid guide">
                @foreach($category->articles as $article)
                <a href="{{ $article->link }}" class="btn btn-primary p-2">
                    <img src="{{ asset('articles/' . $article->image) }}"
                    alt="{{ $article->meta_description }}" width="50" height="50" />
                    <div class="card-body">
                        <h2 class="card-title">{{ $article->title }}</h2>
                        <p class="card-text">{{ $article->description }}</p>
                    </div>
                </a>
                @endforeach
            </div>
        </section>
    </div>
</main>
@stop
