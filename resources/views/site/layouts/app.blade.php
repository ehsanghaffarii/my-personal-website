<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title','احسان غفار | برنامه نویسی وب')</title>
        @include('site.layouts.meta-tags')
        <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/ico">
        <link rel="stylesheet" href="{{ asset('css/app.min.css')}}">
        <link rel="stylesheet" href="{{ asset('css/main.css')}}">
        <!--Pinterest-->
        <meta name="p:domain_verify" content="601d8cc0d17f56ab69a96ca89f96963a"/>
        <!--Facebook pages-->
        <meta property="fb:pages" content="110771900366292" />
        @laravelPWA
        <script>typeof window !== 'undefined' && localStorage.getItem('theme') === 'dark' ? '☀️' : '🌙'</script>
        @yield('styles')
    </head>

    <body>
        <div id="app">

            @include('site.layouts.nav')

            @yield('content')

            @include('site.layouts.footer')
        </div>
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('serviceworker.js') }}"></script>
    @yield('scripts')
    </body>

</html>
