<aside>
    <div class="aside-content">
        @if (isset($article))
      <section>
        <img src="{{ asset('articles/'.$article->image) }}" alt="{{ $article->meta_description }}" class="card-img-top" />
        <p class="mt-3">
          {{ $article->description }}
        </p>
      </section>
      <section>
        <div class="list">
                <h3>امتیاز</h3>
            @for($i=0;$i<$article->rank;$i++)
                <img src="{{ asset('img/icons/star.svg') }}" alt="star" width="20" />
            @endfor
        </div>
      </section>
      <section>
          <h3>دسته‌بندی</h3>
            <p class="">{{ $article->category ? $article->category->name : 'Uncategorized' }}</p>
      </section>
      <section>
        <h3>تاریخ مقاله</h3>
        <small style="font-size: 14px">{{ jdate($article->created_at)->format('Y/m/d') }}</small>
      </section>
      @else
      <section>
        <img src="{{ asset('portfolios/'.$portfolio->image) }}" alt="{{ $portfolio->meta_description }}" class="avatar" />
        <p>
          {{ $portfolio->description }}
        </p>
      </section>
      {{-- <section>
        <h3>تاریخ ساخت</h3>
        <time>{{ $portfolio->completion_date }}</time>
      </section> --}}
      <section>
        <div class="list">
                <h3>امتیاز</h3>
            @for($i=0;$i<$portfolio->rank;$i++)
                <img src="{{ asset('img/icons/star.svg') }}" alt="star" width="20" />
            @endfor
        </div>
      </section>
      @endif
      <section>
        <h3>در تماس باشیم</h3>
        <nav>
          <a
            href="#"
            target="_blank"
            rel="noreferrer"
            class="link"
            ><span class="emoji">✉️</span>ارسال ایمیل</a
          ><a
            target="_blank"
            rel="noreferrer"
            class="link"
            href="#"
            ><span class="emoji">☢️</span>صفحات مجازی</a
          ><a
            href="https://github.com/ehsanghaffarii"
            target="_blank"
            rel="noreferrer"
            class="link"
            ><span class="emoji">🐙</span> گیت‌هاب: @ehsanghaffarii</a
          ><a
            href="https://twitter.com/ehsanghaffarii"
            target="_blank"
            rel="noreferrer"
            class="link"
            ><span class="emoji">🐦</span> توئیتر: @ehsanghaffarii</a
          >
        </nav>
      </section>
    </div>
  </aside>
