<footer class="footer flex">
    <section class="container">
      <nav class="footer-links">
        <a href="{{ route('viewArticles') }}">وبلاگ</a>
        <a href="{{ route('viewPortfolios') }}">نمونه&zwnj;کارها</a>
        <a href="{{ route('viewContactPage') }}">درباره</a>
        <a href="https://ehsanghaffarii.ir/feed">RSS</a>
      </nav>
      <nav class="flex justify-center">
        <a href="https://laravel.com" title="ساخته‌شده با لاراول" target="_blank" rel="noopener noreferrer" class="img">
            <img src="{{ asset('img/icons/web-dev/laravel-226015.svg') }}" alt="فریمورک لاراول" />
          </a><a href="https://linkedin.com/in/ehsanghaffarii" title="صفحه لینکدین احسان غفار" target="_blank" rel="noopener noreferrer" class="img">
            <img src="{{ asset('img/icons/social/027-linkedin.svg') }}" alt="صفحه لینکدین احسان غفارلنگرودی | توسعه دهنده وب" />
        </a><a href="https://instagram.com/ehsanghaffarii" title="صفحه اینستاگرام احسان غفار" target="_blank" rel="noopener noreferrer" class="img">
            <img src="{{ asset('img/icons/social/029-instagram.svg') }}" alt="اینستاگرام توسعه‌دهنده وب" />
        </a>
        <a href="https://dev.to/ehsanghaffarii">
            <img src="https://d2fltix0v2e0sb.cloudfront.net/dev-badge.svg" alt="Ehsan Ghaffarii's DEV Community Profile" height="30" width="30">
        </a>

      </nav>
    </section>
</footer>
