<header>
    <section>
        <div class="container text-center">
        @if(isset($articles))
        <h1>بخش مقالات وبسایت</h1>
        <p class="subtitle">
            مقالات مربوط به طراحی، راه‌اندازی و توسعه‌ی وبسایت
        </p>
        <label for="search">جستجو در مقالات</label>
        <input type="search" name="search" id="search" placeholder="جستجو کن ..." value="" />
        @elseif(isset($category))
        <h1>مقالات مربوط به {{ $category->name }}</h1>
        <p class="subtitle">
            مقالات مربوط به طراحی، راه‌اندازی و توسعه‌ی وبسایت
        </p>
        @elseif(isset($portfolios))
        <h1>نمونه‌کارهای من</h1>
        <p class="subtitle">
            در این بخش نمونه‌کارهای انجام شده قابل مشاهده است
        </p>
        @endif
    </div>
    </section>
</header>
