    <!-- Meta Tags-->
    <meta name="keywords" content="@yield('meta_keywords','برنامه‌نویس')" />
    <meta name="description" content="@yield('meta_description','احسان غفارلنگرودی |‌ برنامه‌نویس و توسعه‌دهنده‌ی وبسایت، این وب‌سایت شخصی بنده است')" />
    <link rel="canonical" href="{{url()->current()}}" />
    <meta name="google-site-verification" content="_Qrw3woXeBrQRCBTuczCQDYemVc92oYarqKJIJy3O88" />
    <meta name="p:domain_verify" content="601d8cc0d17f56ab69a96ca89f96963a"/>
    <!-- OpenGraph-->
    <meta property="og:title" content="@yield('title', 'وبسایت شخصی احسان غفارلنگرودی')" />
    <meta property="og:description" content="@yield('meta_description', 'طراحی و برنامه‌نویسی وبسایت به صورت حرفه ای')" />
    <meta property="og:type" content="Personal blog" />
    <meta property="og:image" content="{{ asset('img/info-img.jpg') }}" />
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:site_name" content="وبسایت احسان غفارلنگرودی" />
    <!-- Twitter-->
    <meta name="twitter:card" content="summery" />
    <meta name="twitter:title" content="@yield('title', 'وبسایت احسان غفارلنگرودی')" />
    <meta name="twitter:description" content="@yield('meta_description', 'وبسایت شخصی احسان غفارلنگرودی، طراح، برنامه‌نویس و توسعه‌دهنده‌ی وبسایت')" />
    <meta name="twitter:image" content="{{ asset('img/info-img.jpg') }}" />
    <meta name="twitter:site" content="@ehsanghaffarii">
    <meta name="twitter:creator" content="@ehsanghaffarii">
    <!--/Meta Tags-->