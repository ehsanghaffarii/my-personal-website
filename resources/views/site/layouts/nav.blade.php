<nav class="navbar bg-primary">
    <div class="container flex-wrap">
        <div class="">
            <a href="/" class="brand">
                <div>
                    <img class="mx-1" src="{{ asset('img/icons/icon-72x72.png') }}" alt="لوگو احسان غفارلنگرودی"/>
                </div>
                <div>
                <p style="font-size: 0.7rem; margin:0">احسان غفارلنگرودی</p>
                <p style="font-size:0.7rem; margin:0; line-height:1">  توسعه‌دهنده وبسایت </p>
                </div>
            </a>
        </div>
        <div class="flex links mx-auto">
            <div class="flex">
                <a href="{{ route('viewPortfolios') }}">نمونه‌کارها</a>
                <a href="{{ route('viewArticles') }}">مقالات</a>
                <a href="{{ route('viewContactPage') }}">درباره‌</a>
                @auth
                <a href="{{ route('admin') }}">ادمین</a></li>
                @endauth
            </div>
        </div>
        <div class="flex">
            <a class="nav-icon" id="dark-mode-button" title="حالت شب/روز"> 🌙 </a>
            <a class="nav-icon" href="https://github.com/ehsanghaffarii" title="صفحه من در گیت‌هاب" rel="noopener noreferrer">
                <img src="{{ asset('img/icons/web-dev/github-34-225988.svg') }}" alt="صفحه گیت هاب احسان غفار برنامه نویس وبسایت" width="30" />
            </a>
        </div>
    </div>
</nav>
