<div>
    @if (count($errors))
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card-title">
        <h4>ارسال پیام به من</h4>
    </div>
    @include('flash::message')
    <form action="{{ route('submitContactMessage') }}" method="post"
        id="contactForm" novalidate="novalidate">
        @csrf
        <div class="d-flex contact-fields py-2">
            <div class="col form-group p-1">
                <input type="text" class="form-control" id="name" name="name" placeholder="اسمت رو وارد کن">
            </div>
            <div class="col form-group p-1">
                <input type="email" class="form-control" id="email" name="email"
                    placeholder="ایمیل یا شماره تلفن">
            </div>
            <div class="col form-group p-1">
                <input type="text" class="form-control" id="subject" name="subject"
                    placeholder="موضوع پیام">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <textarea class="form-control" name="message" id="message" rows="6"
                    placeholder="اینجا پیامتو بنویس"></textarea>
            </div>
        </div>
        <div class="col mt-2">
            <button type="submit" value="submit" class="btn btn-primary btn-block text-white"><span>
            ارسال پیام
            </span></button>
        </div>
    </form>
    </div>
