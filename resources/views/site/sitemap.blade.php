<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://ehsanghaffarii.ir</loc>
    </url>
    <url>
        <loc>https://ehsanghaffarii.ir/article</loc>
    </url>
    <url>
        <loc>https://ehsanghaffarii.ir/portfolio</loc>
    </url>
    <url>
        <loc>https://ehsanghaffarii.ir/about</loc>
    </url>
    @foreach ($articles as $article)
        <url>
            <loc>{{url($article->link)}}</loc>
            <lastmod>{{ gmdate('Y-m-d\TH:i:s\Z',strtotime($article->updated_at)) }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
    @foreach ($portfolios as $portfolio)
        <url>
            <loc>{{url('portfolios/'.$portfolio->slug)}}</loc>
            <lastmod>{{ gmdate('Y-m-d\TH:i:s\Z',strtotime($portfolio->updated_at)) }}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
        @endforeach
</urlset>
