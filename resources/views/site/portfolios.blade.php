@extends('site.layouts.app')
@section('title', 'نمونه کارهای احسان غفارلنگرودی')
@section('meta_description', 'پروژه های انجام شده توسط احسان غفار، وبسایت و وب اپ و ...')



@section('content')

    <main>
        @include('site.layouts.header-section')
        <section>
            <div class="guides-section">
                <div class="container">
                    <section>
                        <section>
                            <h2>
                                نمونه‌کارهای طراحی وبسایت
                            </h2>
                            <div class="grid guide">
                                @foreach ($portfolios as $portfolio)
                                    <a href="{{ route('viewPortfolioPage',[$portfolio->slug]) }}" class="border">
                                        <div class="flex m-2">
                                            <img class="mx-2" width="100" aria-hidden="true"
                                                src="{{ asset('portfolios/' . $portfolio->image) }}"
                                                alt="{{ $portfolio->meta_description }}" />
                                            <h2>{{ $portfolio->title }}</h2>
                                        </div>
                                        <small>آدرس سایت: {{ $portfolio->link }}</small>
                                        <p class="description mt-2">
                                            {{ $portfolio->description }}
                                        </p>
                                    </a>
                                @endforeach
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        </section>
    </main>
@stop
