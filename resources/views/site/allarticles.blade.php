@extends('site.layouts.app')
@section('meta_description', 'این صفحه مقالات در زمینه طراحی سایت و برنامه نویسی وبسایت، همچنین تجربیات خودم در این زمینه را قرار داده‌ام. ')
@section('title', ' مقالات برنامه‌نویسی | وبسایت عین Ein')



@section('content')

    <main>
        @include('site.layouts.header-section')
        <section>
            <div class="guides-section">
                <div class="container">
                    <section>
                        <section>
                            <h2> دسته‌بندی‌ها </h2>
                            <div class="grid guide categories">
                                @foreach ($categories as $category)
                                    <a class="btn btn-primary p-3" href="{{ $category->getLink() }}">{{ $category->name}}</a>
                                @endforeach
                            </div>
                        </section>
                        <section>
                            <h2>جدیدترین ها</h2>
                            <div class="grid guide">
                                @foreach ($articles as $article)
                                    @if ($loop->index < 3 )
                                    <a href="{{ route('viewArticlePage', [$article->slug]) }}">
                                        <div class="flex m-2">
                                            <img class="mx-2" width="100" aria-hidden="true"
                                                src="{{ asset('articles/' . $article->image) }}"
                                                alt="{{ $article->meta_description }}" />

                                            <h2>{{ $article->title }}</h2>
                                        </div>
                                        <p class="description">
                                            {{ $article->description }}
                                        </p>
                                    </a>
                                    @endif
                                @endforeach
                            </div>
                            {{-- paginate --}}
                            {{-- {{ $articles ?? ''->links() }} --}}
                        </section>
                        <section>
                            <h2>همه‌ی مقالات</h2>
                            <div class="grid posts">
                                @foreach($articles ?? '' as $article)
                                <div class="">
                                    <a class="cell" href="{{ route('viewArticlePage', [ $article->slug]) }}">
                                        <div>
                                            <time>{{ jdate($article->created_at) }}</time>
                                            <div>
                                                {{ $article->title }}
                                                <p><small>{{ $article->category->name }}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                        </section>
                    </section>
                </div>
            </div>
        </section>
    </main>
@stop
