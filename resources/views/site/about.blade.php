@extends('site.layouts.app')
@section('title', 'درباره من | احسان غفارلنگرودی توسعه‌دهنده سایت')

@section('content')

<main>
    <header>
      <div class="container medium text-center">
        <h1>درباره من</h1>
      </div>
    </header>
    <div class="container medium">
          <section>
                <div>
            <div class="image mb-3">
                <img class="rounded-lg" src="img/Ehsanghaffarii.jpg" alt="{{ settings('title') }}" />
            </div>
            <div class="note">
            <p>
                سلام! من احسان غفارلنگرودی هستم
            </p>
            <p>
                من در حال حاضر به عنوان توسعه دهنده وبسایت کار می کنم و برای سرگرمی پروژه های منبع باز(Open Source) می سازم. این وب سایت شخصی من است که در آن همه آنچه را می دانم به اشتراک می گذارم
            </p>
            <p>
                امیدوارم که سایت را دوست داشته باشید و اگر موردی وجود دارد که بخواهید با من صحبت کنید، در صورت تمایل از طریق ایمیل برای من بنویسید.
            </p>
            <p>
                خوشحالم که نظرات، بازخورد ها، پیشنهادات شما را می‌خوانم! (ایمیل های مربوط به تبلیغات ، ارسال های تبلیغاتی ، اشتراک لینک و غیره حذف و اسپم خواهند‌شد.)
            </p>
            </div>
            </div>
          </section>
            <section>
                <h2> تجربه‌های کاری من </h2>
                <ul>
                    <li>شرکت آشیانه‌پایدار‌قرن  سال ۱۳۹۲ تا ۱۳۹۶ </li>
                    <li> شرکت پنجره گلدوین سال ۱۳۸۸ تا کنون</li>
                    <li> شرکت فناورپژوهان‌عصر‌امین سال ۱۳۹۷ تا کنون </li>
                </ul>
            </section>
            <section>
                <h2> مشخصات سیستم من </h2>
                <ul>
                    <li> کامپیوتر: <span> مک‌بوک ایر </span></li>
                    <li> کد ادیتور: <span> VSCode </span></li>

                </ul>
            </section>
            <section>
                <h2> شبکه‌های اجتماعی </h2>
                <div>
                    <!--<div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="en_US" data-type="vertical" data-theme="dark" data-vanity="ehsanghaffarii"><a class="LI-simple-link" href='https://ir.linkedin.com/in/ehsanghaffarii?trk=profile-badge'>Ehsan Ghaffarii</a></div>-->
                    <a href="https://linkedin.com/in/ehsanghaffarii" rel="nofollow" class="">
                        <img src="/img/ehsanghaffarii-linkedin-account.png" alt="صفحه لینکدین احسان غفارلنگرودی" />
                    </a>
                </div>
            </section>
            <section>
                @include('site.layouts.contact-form')
            </section>
        </div>
</main>

@stop
