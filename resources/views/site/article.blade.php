@extends('site.layouts.app')

@section('meta_description', $article->meta_description)
@section('title', $article->title)


@section('content')
    <main>
        <div class="container">
            <section class="grid post">
              <article>
                <header class="article-header">
                  <div class="container">
                    <div class="thumb">
                      <h1>
                        {{ $article->title }}
                      </h1>
                    </div>
                  </div>
                </header>
                <div class="banner_content text-right">
                    <div class="breadcrumb breadcrumb-gray">
                        <a class="breadcrumb-item" href="{{ route('homePage') }}">خانه</a>
                        <a class="breadcrumb-item" href="{{ route('viewArticles') }}">مقالات</a>
                        <a class="breadcrumb-item active">{{ $article->title }}</a>
                    </div>
                </div>
                <div>
                  <p class="article-content">
                    {!! $article->content !!}
                  </p>
                </div>
              </article>
              @include('site.layouts.aside')
            </section>
            <nav class="flex container">
            <a class="btn btn-secondary" rel="prev" href="{{ url('article/') }}"><span></span> بازگشت به مقالات</a>
            {{-- <a rel="next" href="{{ url('articles/' . $article->prev ? 'prev' : 'next') }}"><span>بعدی</span>تیتر مقاله بعدی</a> --}}
            </nav>
        </div>
    </main>
@stop
