@extends('site.layouts.app')
@section('title', 'وبسایت احسان غفارلنگرودی')
@section('meta_description', ' اینجا خانه‌ی دیجیتال یک برنامه‌نویس وبسایت است ')

@section('content')
    <main>
        <!-- about me section -->
        <section class="lead">
            <div class="container">
                <div class="copy">
                    <h1>
                        {{ settings('title') }}
                    </h1>
                    <p>
                        {!! settings('description') !!}
                    </p>
                    <h2 style="font-size: 16px">
                        مهارت‌های من:

                    </h2>
                    <div class="nav mb-2">
                        <img class="skill-list mx-3" src="{{ asset('img/icons/web-dev/bootstrap-226077.svg') }}" alt="بوت استرپ" />
                        <img class="skill-list mx-3" src="{{ asset('img/icons/web-dev/javascript-1-225993.svg') }}" alt="برنامه نویس جاوااسکریپت" />
                        <img class="skill-list mx-3" src="{{ asset('img/icons/web-dev/laravel-226015.svg') }}" alt="توسعه دهنده لاراول" />
                        <img class="skill-list mx-3" src="{{ asset('img/icons/web-dev/wordpress-1-226061.svg') }}" alt="توسعه دهنده وردپرس" />
                        <img class="skill-list mx-3" src="{{ asset('img/icons/web-dev/git-225996.svg') }}" alt="سیستم کنترل ورژن گیت" />
                    </div>
                    <!--<small class="bg-danger text-white">-->
                    <!--    این وبسایت همچنان در حال طراحی است و به صورت تست راه‌اندازی شده است-->
                    <!--</small>-->
                </div>
                <div class="image">

                    <img src="img/{{ settings('image') }}" alt="{{ settings('title') }}" />
                </div>
            </div>
        </section>

        <div class="container index mt-4">

            <!-- article section -->
            <section>
                <h2>جدیدترین مقالات<a href="{{ route('viewArticles') }}" class="section-button">دیدن همه</a></h2>
                <div class="grid guide">
                @foreach ($articles as $article)
                    {{-- @if ($loop->index < 3 ) --}}
                    {{-- <a href="{{  $article->slug }}"> --}}
                    <a href="{{ route('viewArticlePage', [$article->slug]) }}">
                    <img src="{{ asset('articles/' . $article->image) }}" alt="{{ $article->meta_description }}" height="50" width="50" />
                    <h2 class="section-title">{{ $article->title }}</h2>
                    <p id="{{ $article->category->slug }}" class="categoryName badge bg-dark text-white p-2 mb-2">{{ $article->category ? $article->category->name : 'Uncategorized' }}</p>
                    <div style="font-size: 12px">
                    @for ($i = 0; $i < $article->rank; $i++)
                        <i class="fa fa-star text-warning"></i>
                    @endfor
                    </div>
                    <p class="description">
                      {{ $article->description }}
                    </p>
                  </a>
                  {{-- @endif --}}
                @endforeach
                </div>
            </section>
            <!-- portfolio Section -->
            <section>
                <h2>نمونه‌کارها<a href="{{ route('viewPortfolios') }}" class="section-button">دیدن همه</a></h2>
                <div class="grid grid posts with-tags">

                @foreach ($portfolios as $portfolio)
                    <div class="row">
                <a href="{{ route('viewPortfolioPage', [$portfolio->slug]) }}" class="cell">
                    <div>
                        <div class="section-title">{{ $portfolio->title }}</div>
                        <small style="font-size:12px">سایت: {{ $portfolio->link }} </small>
                        <div class="mt-2">
                            <p>{{ $portfolio->description }}</p>
                        </div>
                    </div>
                </a>
                    </div>
                @endforeach
                </div>
            </section>
            <section>
                <h2>پروژه‌های متن‌باز</h2>
                <div class="grid projects">
                    <a href="#" class="row" target="_blank" rel="noreferrer">
                    <div class="cell simple"><i class="fab fa-bootstrap fa-2x"></i></div>
                    <div class="cell simple">قالب وردپرس عین‌بلاگ</div>
                    <div class="cell simple light description">
                        <p>
                        قالب برنامه‌نویسی شده با استفاده از بوت‌استرپ ۴
                        </p>
                    </div>
                    </a>
                        <a href="#" class="row" target="_blank" rel="noreferrer">
                        <div class="cell simple"><i class="fab fa-js fa-2x"></i></div>
                        <div class="cell simple">مدرن استاتیک سایت</div>
                        <div class="cell simple light description">
                            <p>
                            ساخت وبسایت استاتیک با استفاده از گتسبای و جاوااسکریپت
                            </p>
                        </div>
                        </a>
                </div>
            </section>
        </div>
    </main>
@stop
