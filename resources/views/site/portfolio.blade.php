@extends('site.layouts.app')
@section('meta_description', $portfolio->meta_description)
@section('title', $portfolio->title)

@section('content')
<main>
    <div class="container">
        <section class="grid post">
          <article>
            <header class="article-header">
              <div class="container">
                <div class="thumb">
                  <h1>
                    {{ $portfolio->title }}
                  </h1>
                </div>
              </div>
            </header>
            <div class="banner_content text-right">
                <div class="breadcrumb breadcrumb-gray">
                    <a class="breadcrumb-item" href="{{ route('homePage') }}">خانه</a>
                    <a class="breadcrumb-item" href="{{ route('viewPortfolios') }}">نمونه‌کارها</a>
                    <a class="breadcrumb-item active">{{ $portfolio->title }}</a>
                </div>
            </div>
            <div>
              <p class="article-content">
                {!! $portfolio->content !!}
              </p>
            </div>
          </article>
          @include('site.layouts.aside')
        </section>
        {{-- <nav class="flex container suggested">
            <a rel="prev" href="#"><span>قبلی</span>تیتر مقاله قبلی</a>
            <a rel="next" href="#"><span>بعدی</span>تیتر مقاله بعدی</a>
        </nav> --}}
    </div>
</main>
@stop
