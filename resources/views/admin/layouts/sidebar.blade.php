<div class="card">
    <div class="card-header">Sidebar</div>
    <div class="card-body">

                @include('flash::message')

        <ul class="navbar navbar-nav">
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="{{ route('admin') }}">Dashboard</a></li>
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="{{ route('viewListArticles') }}">Article</a></li>
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="{{ route('viewListPortfolios') }}">Portfolios</a></li>
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="{{ route('viewAboutMe') }}">About Me</a></li>
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="{{ route('viewMessages') }}">Messages</a></li>
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="{{ route('viewCategories') }}">Categories</a></li>
            <li class="nav-item p-2 m-2 border-bottom"><a class="nav-link" href="#">Settings</a></li>
        </ul>
    </div>
</div>
