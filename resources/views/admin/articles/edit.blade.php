@extends('admin.layouts.panel')

@section('content')
    <div class="card">
        <div class="card-header">Edit article</div>

        <div class="card-body">

                    @include('flash::message')
            @if (count($errors))
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('editArticle', ['article' => $article]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" value="{{ $article->title }}" id="title" name="title" aria-describedby="titleHelp">
                    <small id="titleHelp" class="form-text text-muted">Choose a title for your article</small>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" name="image">
                    <label class="custom-file-label" for="image">Choose image</label>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" rows="3">{{ $article->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select class="form-control" name="category_id" required>
                        <option value="">Select a Category</option>

                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{ $category->id === old('category_id') ? 'selected' : '' }}>{{ $category->name }}</option>
                            @if ($category->children)
                                @foreach ($category->children as $child)
                                    <option value="{{ $child->id }}" {{ $child->id === old('category_id') ? 'selected' : '' }}>&nbsp;&nbsp;{{ $child->name }}</option>
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="rank">Rank</label>
                    <select class="custom-select" id="rank" name="rank">
                        <option value="1" @if($article->rank == 1) {{'selected'}} @endif>1</option>
                        <option value="2" @if($article->rank == 2) {{'selected'}} @endif>2</option>
                        <option value="3" @if($article->rank == 3) {{'selected'}} @endif>3</option>
                        <option value="4" @if($article->rank == 4) {{'selected'}} @endif>4</option>
                        <option value="5" @if($article->rank == 5) {{'selected'}} @endif>5</option>
                    </select>
                </div>
                {{-- <div class="form-group">
                    <label for="client">Client</label>
                    <input type="text" class="form-control" id="client" value="{{ $article->client }}" name="client">
                </div> --}}
                <div class="form-group">
                    <label for="completion_date">Completion Date</label>
                    <input type="text" class="form-control" id="completion_date" value="{{ jdate($article->completion_date)->format('Y/m/d') }}" name="completion_date"
                           aria-describedby="completion_dateHelp">
                    <small id="completion_dateHelp" class="form-text text-muted">Attention just enter like:
                        1398/12/02</small>
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" id="content" name="content" rows="10">{{ $article->content }}</textarea>
                </div>
                <div class="form-group">
                    <label>slug</label>
                    <input name="slug" class="form-control" value="{{ $article->slug }}">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <input name="meta_description" class="form-control" value="{{ $article->meta_description }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@stop
