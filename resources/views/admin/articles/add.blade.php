@extends('admin.layouts.panel')

@section('content')
<div class="card">
    <div class="card-header">Add Article</div>

    <div class="card-body">
                @include('flash::message')

        <form action="{{ route('addArticle') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp">
                <small id="titleHelp" class="form-text text-muted">Choose a title for your Article</small>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image">
                <label class="custom-file-label" for="image">Choose image</label>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="category_id">Category</label>
                <select class="form-control" name="category_id" required>
                    <option value="">Select a Category</option>

                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ $category->id === old('category_id') ? 'selected' : '' }}>{{ $category->name }}</option>
                        @if ($category->children)
                            @foreach ($category->children as $child)
                                <option value="{{ $child->id }}" {{ $child->id === old('category_id') ? 'selected' : '' }}>&nbsp;&nbsp;{{ $child->name }}</option>
                            @endforeach
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="rank">Rank</label>
                <select class="custom-select" id="rank" name="rank">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" id="content" name="content" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="slug">slug</label>
                <input name="slug" class="form-control">
            </div>
            <div class="form-group">
                <label for="author">author</label>
                <input name="author" class="form-control">
            </div>
            <div class="form-group">
                <label for="completion_date">Completion Date</label>
                <input type="text" class="form-control" id="completion_date" name="completion_date" aria-describedby="completion_dateHelp">
                <small id="completion_dateHelp" class="form-text text-muted">Attention just enter like: 1398/12/02</small>
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <input name="meta_description" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@if (count($errors))
<div class="alert alert-danger" role="alert">
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@stop
