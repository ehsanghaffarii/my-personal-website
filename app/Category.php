<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{

    protected $fillable = ['parent_id', 'name', 'slug'];

    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'slug',
            ]
        ];
    }

    public function getLink()
    {
        return url('categories/'.$this->slug);
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

}
