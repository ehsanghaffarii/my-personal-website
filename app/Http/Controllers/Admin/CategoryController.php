<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Validation\Rule;
use Morilog\Jalali\Jalalian;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::with('children')->whereNull('parent_id')->get();

        return view('admin.categories.index')->with([
            'categories' => $categories
        ]);
    }

    public function store(Request $request)
{
      $validatedData = $this->validate($request, [
            'name' => 'required|min:3|max:255|string',
            'slug' => 'required|min:3|max:255|string',
            'parent_id' => 'sometimes|nullable|numeric'
      ]);

      Category::create($validatedData);

      return redirect()->route('viewCategories')->withSuccess('notification', 'You have successfully created a Category!');
}

public function update(Request $request, Category $category)
{
        $validatedData = $this->validate($request, [
            'name'  => 'required|min:3|max:255|string',
            'slug' => 'required|min:3|max:255|string'
        ]);

        $category->update($validatedData);

        return redirect()->route('admin.categories.index')->with('notification', 'You have successfully updated a Category!');
}

public function destroy(Category $category)
{
        if ($category->children) {
            foreach ($category->children()->with('articles')->get() as $child) {
                foreach ($child->articles as $article) {
                    $article->update(['category_id' => NULL]);
                }
            }

            $category->children()->delete();
        }

        foreach ($category->articles as $article) {
            $article->update(['category_id' => NULL]);
        }

        $category->delete();

        return redirect()->route('category.index')->withSuccess('You have successfully deleted a Category!');
}


}
