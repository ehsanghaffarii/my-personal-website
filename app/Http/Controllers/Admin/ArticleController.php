<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Article;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Morilog\Jalali\Jalalian;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Http\Controllers\Admin\CategoryController;
use Illuminate\Support\Str;


class ArticleController extends Controller
{
    public function articles()
    {
        $articles = Article::query()->orderBy('created_at', 'desc')->get();
        return view('admin.articles.list', ['articles' => $articles]);
    }

    public function view_add(Article $Article, Category $categories)
    {
        $categories = Category::with('children')->whereNull('parent_id')->get();

        return view('admin.articles.add', ['Article' => $Article], ['categories' => $categories]);

    }

    public function add(Request $request)
    {
        $request->validate([
            'slug' => 'required|unique:articles',
            'title' => 'required|max:255',
            'image' => 'required',
            'description' => 'required',
            'content' => 'required',
            'author' => 'required',
            'rank' => ['required', Rule::in(['1', '2', '3', '4', '5'])],
            'meta_description' => 'required|max:255',
            'category_id' => 'required|numeric',

        ]);

        $article = Article::query()->create([
            'slug' => $request->slug,
            'title' => $request->title,
            'image' => 'test',
            'description' => $request->description,
            'content' => \request('content'),
            'author' => $request->author,
            'rank' => $request->rank,
            'meta_description' => $request->meta_description,
            'category_id' => $request->category_id,
        ]);


        $image = $request->file('image');
        $filename = $article->id .'_'. time() .'.'. $image->getClientOriginalExtension();
        $image->move('articles', $filename);
        $article->image = $filename;
        $article->save();

        flash('Article Created!')->success();
        return redirect(route('viewAddArticle', [$article->slug]))->with('notification', 'Article Created!');
    }

//     public function create()
// {
//       $categories = Category::with('children')->whereNull('parent_id')->get();

//       return view('post.create')->withCategories($categories);
// }


    // edit article view
    public function view_edit(Article $Article, Category $categories) {

        $categories = Category::with('children')->whereNull('parent_id')->get();
        return view('admin.articles.edit', ['article' => $Article], ['categories' => $categories]);
    }

    // edit article
    public function edit(Request $request, Article $article, Category $categories)
    {
        $request->validate([
            'slug' => 'required|unique:articles,id,',
            'title' => 'required|max:255|unique:articles,id,',
            'description' => 'required',
            'content' => 'required',
            'rank' => ['required', Rule::in(['1', '2', '3', '4', '5'])],
            'meta_description' => 'required|max:255',
            'category_id' => 'required|numeric',
        ]);


        $article->update([
            'slug' => $request->slug,
            'title' => $request->title,
            'description' => $request->description,
            'rank' => $request->rank,
            'completion_date' => Jalalian::fromFormat('Y/m/d', $request->completion_date)->toCarbon(),
            'content' => \request('content'),
            'meta_description' => $request->meta_description,
            'category_id' => $request->category_id,
        ]);


        if ($request->hasFile('image')){
            File::delete(public_path().'/article/'.$article->image);

            $image = $request->file('image');
            $filename = $article->id . '_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move('articles', $filename);
            $article->image = $filename;
            $article->save();

        }

        flash('Article update')->success();
        return redirect(route('viewListArticles'));
    }
    public function delete(Article $article)
    {
        File::delete(public_path().'/articles/'.$article->image);
        $article->delete();

        flash('Article deleted')->warning();
        return redirect(route('viewListArticles'));
    }

}
