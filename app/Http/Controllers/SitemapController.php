<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Portfolio;

class SitemapController extends Controller
{
    public function index(Request $request)
    {
        $articles = Article::orderBy('id', 'desc')->get();
        $portfolios = Portfolio::orderBy('id', 'desc')->get();
        return response()->view('site.sitemap', compact('articles', 'portfolios'))
            ->header('Content-Type', 'text/xml');
    }
}
