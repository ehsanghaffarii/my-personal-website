<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use App\Contact;
use App\Article;
use App\Category;

class SiteController extends Controller
{
    // home controller
    public function home()
    {
        $portfolios = Portfolio::query()->orderBy('created_at', 'desc')->get();
        $articles = Article::query()->orderBy('created_at', 'desc')->get();

        return view('site.home', ['portfolios' => $portfolios] + ['articles' => $articles]);
    }

    // single portfolio
    public function portfolio_detail($slug)
    {

        $portfolio = Portfolio::where('slug', $slug)->first();
        return view('site.portfolio', compact('portfolio'));
    }

    // all portfolios
    public function portfolios_all()
    {
        $portfolios = Portfolio::query()->orderBy('created_at', 'desc')->get();

        return view('site.portfolios', ['portfolios' => $portfolios]);

    }

    // all articles
    public function articles_all()
    {
        $categories = Category::query()->orderBy('created_at', 'desc')->get();

        $articles = Article::query()->orderBy('created_at', 'desc')->get();
        return view('site.allarticles', compact('articles', 'categories'));

    }

    // single article
    public function article_page($slug)

    {
        $article = Article::where('slug', $slug)->first();

        return view('site.article', compact('article'));
    }

    // contact
    public function contact()
    {
        return view('site.about');
    }

    // comment
    public function submit_contact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        Contact::create($request->except('_token'));
        flash('پیام ارسال شد')->success();
        return redirect()->route('viewContactPage');
    }

    // public function articles() {

    //     $category = new Category([
    //         'name' => 'category',
    //         'description' => 'category description',
    //         'slug' => 'category slug',
    //         'handle' => 'category handle',
    //     ]);
    //     $category->save();

    //     $data = Article::with('category')->paginate(10);
    //     return view('site.posts', compact('data'));
    // }

    public function allCategories() {

        $categories = Category::all();

        return view('site.allCategories', compact('categories'));
    }

    public function singleCategory($slug) {

        $category = Category::with('articles')->where('slug', $slug)->first();

        return view('site.category', compact('category'));
    }

    // public function categoryArticles(){

	// 	$category = Category::with('articles')->where('slug')->get();
	// 	return view('site.category', compact('category'));
	// }

	// public function articleDetails($slug){

	// 	$data = Category::with('category')->where('slug',$slug)->first();
	// 	return view('site.article-details',compact('data'));
	// }


}
