<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class portfolio extends Model
{
    protected $fillable = ['title', 'image', 'description', 'rank', 'client', 'link', 'completion_date', 'content', 'slug', 'meta_description'];


    public function getRouteKeyName()
    {
        return 'slug';
    }

}
