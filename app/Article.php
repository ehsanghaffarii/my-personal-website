<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = ['category_id', 'user_id', 'slug', 'title', 'image', 'description', 'content', 'author', 'rank', 'meta_description', 'completion_date'];

    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'slug',
            ]
        ];
    }

    public function getLinkAttribute()
    {
        return route('viewArticlePage', ['article_by_slug' => $this->slug]);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}

