
// dark theme script
const darkTheme = (event) => {
    const theme =
        typeof window !== "undefined" && localStorage.getItem("theme");

    if (theme === "dark") {
        typeof window !== "undefined" && localStorage.removeItem("theme");
        const link = document.querySelectorAll("#dark-mode");

        if (link) {
            link.forEach(el => el.remove());
            event.target.textContent = '🌙'
        }
    } else {
        typeof window !== "undefined" && localStorage.setItem("theme", "dark");
        event.target.textContent = '☀️'
        const head = document.getElementsByTagName("head")[0];
        const link = document.createElement("link");
        link.rel = "stylesheet";
        link.id = "dark-mode";
        link.href = window.location.origin + "/css/dark.css";

        head.appendChild(link);
    };
};

document.getElementById('dark-mode-button').addEventListener('click', darkTheme)
