<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

// admin routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('admin');

    Route::group(['prefix' => 'portfolios'], function () {
        Route::get('/', 'PortfolioController@portfolios')->name('viewListPortfolios');
        Route::get('/add', 'PortfolioController@view_add')->name('viewAddPortfolio');
        Route::post('/add', 'PortfolioController@add')->name('addPortfolio');
        Route::get('/edit/{portfolio}', 'PortfolioController@view_edit')->name('viewEditPortfolio');
        Route::post('/edit/{portfolio}', 'PortfolioController@edit')->name('editPortfolio');
        Route::delete('/delete/{portfolio}', 'PortfolioController@delete')->name('deletePortfolio');

    });

    Route::group(['prefix' => 'articles'], function () {
        Route::get('/', 'ArticleController@articles')->name('viewListArticles');
        Route::get('/add', 'ArticleController@view_add')->name('viewAddArticle');
        Route::post('/add', 'ArticleController@add')->name('addArticle');
        Route::get('/edit/{article}', 'ArticleController@view_edit')->name('viewEditArticle');
        Route::post('/edit/{article}', 'ArticleController@edit')->name('editArticle');
        Route::delete('/delete/{article}', 'ArticleController@delete')->name('deleteArticle');
    });

    // about_me routes
    Route::get('/about_me', 'DashboardController@view_about_me')->name('viewAboutMe');
    Route::post('/about_me', 'DashboardController@save_about_me')->name('saveAboutMe');

    // show messages
    Route::get('/messages', 'DashboardController@view_messages')->name('viewMessages');
    Route::get('/message/{contact}', 'DashboardController@view_single_message')->name('viewSingleMessage');

    // categories controller
    Route::resource('category', 'CategoryController');
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoryController@index')->name('viewCategories');
        Route::post('/add', 'CategoryController@store')->name('addCategoriy');
        Route::put('/edit', 'CategoryController@update')->name('updateCategoriy');
    });

});

// website routes
Route::get('/', 'SiteController@home')->name('homePage');
Route::get('/about', 'SiteController@contact')->name('viewContactPage');
Route::get('portfolios/{portfolio_by_slug}', 'SiteController@portfolio_detail')->name('viewPortfolioPage');
Route::post('/contact', 'SiteController@submit_contact')->name('submitContactMessage');
Route::get('articles/{slug}', 'SiteController@article_page')->name('viewArticlePage');
Route::get('article', 'SiteController@articles_all')->name('viewArticles');
Route::get('/portfolio', 'SiteController@portfolios_all')->name('viewPortfolios');
Route::get('sitemap.xml','SitemapController@index');

Route::get('/category', 'SiteController@allCategories')->name('allCategories');
Route::get('/categories/{slug}', 'SiteController@singleCategory')->name('viewCategory');

